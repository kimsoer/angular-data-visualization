import { Component, OnInit,ViewEncapsulation } from '@angular/core';
import { Observable } from 'rxjs';
import { Province, Provinces } from 'src/app/model/provinces';
import { RemoteService } from 'src/app/service/remote.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
 provinces? : Observable<Provinces>;
 
 communeValue?:String
 currentIndex? :number
 provinceList? : Province[];
 province?: Province;

 provinceUpdate : any;
 provinceData:any;

  constructor(private service:RemoteService, private modalService: NgbModal) {
   }
  

  ngOnInit(): void {
    this.getAllData();
   
  }
  openXl(content :any, index :any) {
    this.province = this.provinceList![index];
    console.log(`index=> ${this.province.YEAR_NAME}`)
    this.currentIndex = index;

    this.modalService.open(content, { size: 'xl' });

  }

  update(province: Province){
    this.service.update(province).subscribe(
      value=> {
        this.modalService.dismissAll()
      }
    )
    
  }
  search(){
    console.log(`data=> ${this.communeValue}`);
    this.service.getSearch(this.communeValue!).subscribe(
      value=> {
        this.provinceList = value.data;
        console.log(`data=> ${value.data}`)
      }
    )

  }
  getAllData(){
    this.provinces = this.service.getDataList();
    this.provinces.subscribe(
      value =>{
        this.provinceList = value.data;

      }
    )
    
  }
}
